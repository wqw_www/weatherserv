package wspackage;



public class Client {

	public static void main(String args[]) {
		TstLoad[] clients = new TstLoad[10];
		for(int i=0; i<clients.length;i++)
			clients[i] = new TstLoad();
		
		for(TstLoad cl : clients){			
			cl.run();
		}
	}
}

class TstLoad implements Runnable{	
	static SayHiWsService service = new SayHiWsService();
	static SayHiWs sayWs = service.getSayHiWsPort();

	@Override
	public void run() {	
		for(int i=0; i<100000; i++)
			System.out.println("run "+i);
			sayWs.getWeather("Madrid", "Spain");				
	}
}
