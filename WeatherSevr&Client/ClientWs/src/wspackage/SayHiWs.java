
package wspackage;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SayHiWs", targetNamespace = "http://wspackage/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SayHiWs {


    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getWeather", targetNamespace = "http://wspackage/", className = "wspackage.GetWeather")
    @ResponseWrapper(localName = "getWeatherResponse", targetNamespace = "http://wspackage/", className = "wspackage.GetWeatherResponse")
    @Action(input = "http://wspackage/SayHiWs/getWeatherRequest", output = "http://wspackage/SayHiWs/getWeatherResponse")
    public String getWeather(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1);

}
